// public/js/classes/NormalRandom.js

define("Tier", [
		'math'
	], function(
		math
	){

	'use strict';
	
	return function(
		newName,
		newType,
		newUnitArray,
		newResearchArray,
		newCurrency,
		newObjective
	){

		var self = this;

		var NUMBER_OF_UNITS = 9;

		var name = newName;
		self.aname = name;
		var type = newType;
		var tier;
		var units = newUnitArray;
		var currency = {};
		currency.amount = math.bignumber(newCurrency);
		var research = newResearchArray;
		switch (type){
			case "Continent":
				tier = 0;
				currency.name = "Population"
				units.names = [
					"Clearing",
					"Village",
					"Town",
					"City",
					"County",
					"Megapolis",
					"State",
					"Nation",
					"Union"
				]
				units.pluralNames = [
					"Clearings",
					"Villages",
					"Towns",
					"Cities",
					"Counties",
					"Megapolis",
					"States",
					"Nations",
					"Unions"
				]
				break;
			case "Satellite":
				tier = 1;
				currency.name = "Data"
				units.names = [
					"Weather Balloon",
					"Reconnaissance Satellite",
					"Meteorological Satellite",
					"Communication Satellite",
					"Video Satellite",
					"Telescope",
					"Space Station",
					"Space Hub",
					"Moon"
				]
				units.pluralNames = [
					"Weather Balloon",
					"Reconnaissance Satellites",
					"Meteorological Satellites",
					"Communication Satellites",
					"Video Satellites",
					"Telescopes",
					"Space Stations",
					"Space Hubs",
					"Moons"
				]
				break;
			case "Planet":
				tier = 2;
				currency.name = "Ore"
				units.names = [
					"Volcano",
					"Island",
					"Sea",
					"Mountain Range",
					"Region",
					"Continent",
					"Satellite",
					"Ocean",
					"Moon"
				]
				units.pluralNames = [
					"Volcanos",
					"Islands",
					"Seas",
					"Mountain Ranges",
					"Regions",
					"Continents",
					"Satellites",
					"Oceans",
					"Moons"
				]
				break;
			case "System":
				tier = 3;
				currency.name = "Metals"
				units.names = [
					"Planetesimal",
					"Dwarf Planet",
					"Asteroid",
					"Terrestrial Planet",
					"Double Planet",
					"Gas Giant",
					"Planetar",
					"Star",
					"Binary Star"
				]
				units.pluralNames = [
					"Planetesimals",
					"Dwarf Planets",
					"Asteroids",
					"Terrestrial Planets",
					"Double Planets",
					"Gas Giants",
					"Planetars",
					"Stars",
					"Binary Stars"
				]
				break;
			case "Sector":
				tier = 4;
				currency.name = "Fuel"
				units.names = [
					"Star",
					"Pulsar",
					"Planetary System",
					"Binary System",
					"Simplex System",
					"Planetary Nebula",
					"Emission Nebula",
					"Interstellar Cloud",
					"Black Hole"
				]
				units.pluralNames = [
					"Stars",
					"Pulsars",
					"Planetary Systems",
					"Binary Systems",
					"Simplex Systems",
					"Planetary Nebulas",
					"Emission Nebulas",
					"Interstellar Clouds",
					"Black Holes"
				]
				break;
			case "Galaxy":
				tier = 5;
				currency.name = "Energy"
				units.names = [
					"Star",
					"Star System",
					"Globular Cluster",
					"Dust Cloud",
					"Gas Cloud",
					"Planetary Nebula",
					"Nuclear Bulge",
					"Arm",
					"Bar"
				]
				units.pluralNames = [
					"Stars",
					"Star Systems",
					"Globular Clusters",
					"Dust Clouds",
					"Gas Clouds",
					"Planetary Nebulas",
					"Sectors",
					"Arms",
					"Bars"
				]
				break;
		}
		if (typeof units.numbers === 'undefined'){
			units.numbers = [];
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				units.numbers.push(math.bignumber(
					research.unitResearch[i].startingNumber.value
				));
			}
		}

		self.updateCost = function(index){
			units.cost[index] = math.ceil(math.add(
				units.cost[index],
				math.multiply(
					math.pow(math.bignumber(3.5), index),
					math.bignumber(units.numbers[index])
				)
			));
			if (math.equal(units.cost[index], 0)){
				units.cost[index] = math.pow(math.bignumber(3), index);
			}
			units.cost[index] = math.ceil(math.multiply(
				units.cost[index],
				math.bignumber(research.unitResearch[index].priceMultiplier.value)
			));
		}
		if (typeof units.cost === 'undefined'){
			units.cost = [];
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				units.cost.push(math.bignumber(0));
				self.updateCost(i);
			}

		}
		if (typeof units.counter === 'undefined'){
			units.counter = [];
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				units.counter.push(math.bignumber(0));
			}
		}
		if (typeof units.max === 'undefined'){
			units.max = [];
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				if (i % 3 == 0){
					units.max[i] = math.multiply(
						math.bignumber(1000 * (math.pow(10, (math.floor(i / 3))))),
						math.bignumber(research.unitResearch[i].timeMultiplier.value)
					)
				}
				if (i % 3 == 1){
					units.max[i] = math.multiply(
						math.bignumber(3000 * (math.pow(10, (math.floor(i / 3))))),
						math.bignumber(research.unitResearch[i].timeMultiplier.value)
					)
				}
				if (i % 3 == 2){
					units.max[i] = math.multiply(
						math.bignumber(4500 * (math.pow(10, (math.floor(i / 3))))),
						math.bignumber(research.unitResearch[i].timeMultiplier.value)
					)
				}
			}
		}
		if (typeof units.produce === 'undefined'){
			units.produce = [];
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				units.produce.push(math.pow(math.bignumber(2), i));
			}
		}

		self.generateObjective = function(){
			var chance = Math.floor(Math.random() * 4);
			switch (chance){
				// Get X currency
				case 0:
					return {
						"type": "currency",
						"amount": math.multiply(
							math.bignumber(10000),
							math.pow(math.bignumber(4), math.add(tier, 1))
						)
					}
					break;
				// Get X units
				case 1:
				return {
						"type": "unit",
						"unit": Math.floor(Math.random() * 8),
						"amount": math.multiply(
							math.bignumber(10),
							math.pow(math.bignumber(4), math.add(tier, 1))
						)
					}
					break;
				// Get X of each unit
				case 2:
					return {
						"type": "each",
						"amount": math.multiply(
							math.bignumber(10),
							math.pow(math.bignumber(4), math.add(tier, 1))
						)
					}
					break;
				// Produce X per second
				case 3:
					return {
						"type": "produce",
						"amount": math.add(
							math.bignumber(300),
							math.ceil(math.pow(math.bignumber(1.75), math.add(tier, 1)))
						)
					}
					break;
			}
		}
		var objective;
		if (typeof newObjective === 'undefined'){
			objective = self.generateObjective();
		} else {
			objective = newObjective;
		}
		objective.completed = false;
		objective.autocomplete = research.generalResearch.autocomplete.value;
		objective.counter = 0;
		objective.max = math.bignumber(
			research.generalResearch.autocompleteTime.value
		);

		self.getSaveString = function(){
			return {
				"name": name,
				"type": type,
				"units": units,
				"research": research,
				"currency": currency.amount,
				"objective": objective
			}
		}

		self.getName = function(){
			return name;
		}

		self.getType = function(){
			return type;
		}

		self.getUnits = function(){
			var unitsArray = [];
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				unitsArray.push({
					"name": units.names[i],
					"amount": units.numbers[i],
					"cost": units.cost[i],
					"produce": units.produce[i],
					"counter": units.counter[i],
					"max": units.max[i]
				})
			}
			return unitsArray;
		}

		self.getPluralNames = function(){
			return units.pluralNames
		}

		self.haveUnit = function(index){
			if (math.larger(units.numbers[index], 0)){
				return true;
			} else {
				return false;
			}
		}

		self.getCounter = function(index){
			return units.counter[index];
		}

		self.getMax = function(index){
			return units.max[index];
		}

		self.getCurrency = function(){
			return currency;
		}

		self.getObjective = function(){
			return objective;
		}

		self.getObjectiveString = function(){
			var string = "Get " + objective.amount + " ";
			switch(objective.type){
				case "currency":
					string += currency.name;
					break;
				case "unit":
					string += units.pluralNames[objective.unit];
					break;
				case "each":
					string +=  "of each unit";
					break;
				case "produce":
					string += currency.name + " per second";
					break;
			}
			return string;
		}

		self.canBuy = function(index){
			if (math.largerEq(currency.amount, units.cost[index])){
				return true;
			} else {
				return false;
			}
		}

		self.isCompleted = function(){
			return objective.completed;
		}

		self.buy = function(index){
			if (self.canBuy(index)){
				units.numbers[index] = math.add(units.numbers[index], 1);
				currency.amount = math.subtract(currency.amount, units.cost[index]);
				self.updateCost(index);
			}
		}

		

		self.updateResearch = function(newResearchArray){
			research = newResearchArray;
			objective.autocomplete = research.generalResearch.autocomplete.value;
			objective.max = math.bignumber(
				research.generalResearch.autocompleteTime.value
			);
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				units.cost[i] = math.ceil(math.multiply(
					units.cost[i],
					math.bignumber(research.unitResearch[i].priceMultiplier.value)
				));
				units.max[i] = math.ceil(math.multiply(
					units.max[i],
					math.bignumber(research.unitResearch[i].timeMultiplier.value)
				))
			}
		}

		self.produce = function(index, amount){
			return math.multiply(
				math.multiply(
					math.bignumber(amount),
					math.pow(math.bignumber(2), index)
				),
				units.numbers[index]
			)
		}

		self.productionPerSecond = function(){
			var amount = math.bignumber(0);
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				amount = math.add(
					amount,
					math.divide(self.produce(i, 1), math.divide(units.max[i], 1000))
				);
			}
			return amount;
		}

		self.increaseCounter = function(number){
			var produced = math.bignumber(0);
			if (objective.autocomplete && objective.completed){
				objective.counter += number;
			}
			if (math.largerEq(objective.counter, objective.max)){
				objective.autocompleteFinished = true;
			}
			for (var i = 0; i < NUMBER_OF_UNITS; i++){
				if (self.haveUnit(i)){
					units.counter[i] = math.add(units.counter[i], number);
					if (math.larger(units.counter[i], units.max[i])){
						var amount = math.floor(math.divide(units.counter[i], units.max[i]));
						produced = math.add(produced, self.produce(i, amount));
						units.counter[i] = math.mod(units.counter[i], units.max[i]);
					}
				}
			}
			currency.amount = math.add(currency.amount, produced);
			if (!objective.completed){
				self.checkObjective();
			}
		}

		self.checkObjective = function(){
			switch(objective.type){
				case "currency":
					if (math.largerEq(currency.amount, objective.amount)){
						objective.completed = true;
					}
					break;
				case "unit":
					if (math.largerEq(units.numbers[objective.unit], objective.amount)){
						objective.completed = true;
					}
					break;
				case "each":
					var isDone = true;
					for (var i = 0; i < NUMBER_OF_UNITS; i++){
						if (math.smaller(units.numbers[i], objective.amount)){
							isDone = false;
							break;
						}
						if (isDone){
							objective.completed = true;
						}
					}
					break;
				case "produce":
					if (math.largerEq(self.productionPerSecond(), objective.amount)){
						objective.completed = true;
					}

					break;
			}
		}

		//TODO: Remove
		self.cheat = function(){
			objective.completed = true;
		}
		
	};

});