// public/js/classes/NormalRandom.js

define("NormalRandom", [
		'math'
	], function(
		math
	){

	'use strict';
	
	return function(lower, upper){

		if ((typeof lower == 'undefined') && (typeof upper == 'undefined')){
			lower = 0;
			upper = 1;
		}


		var numbers = {
			x : math.random(),
			y : math.random()
		}


		var s = math.eval('sqrt(-2 * log(x)) * cos(2 * y * PI)', numbers);
		var t = math.eval('sqrt(-2 * log(x)) * sin(2 * y * PI)', numbers);

		// s and t both are approx. normally-distributed, so randomly pick one
		numbers = {
			a : math.abs(s),
			b : math.abs(t),
			c : upper,
			d : lower
		} 
		
		var returnVal;
		if (Math.round(Math.random()) == 1){
			returnVal = math.eval('a * (c - d)', numbers);
		} else {
			returnVal = math.eval('b * (c - d)', numbers);
		}		

		return returnVal;

	};

});