// public/js/app.js

define([
    'angular',
    'ngStorage',
    'ui-router',
    'ui-bootstrap',
    'classes',
    'constantsModule',
    'services',
    'controllers'
    ], function(
        angular,
        ngStorage
    ){

    'use strict';

    return angular.module('paxCosmica', [
        'ui.router',
        'ui.bootstrap',
        'ngStorage',
        'paxCosmica.constants',
        'paxCosmica.services',
        'paxCosmica.controllers'
    ])

});