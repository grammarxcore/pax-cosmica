// public/js/constants/module.js

define([
	'angular'
	], function (
		angular
	) {
    'use strict';

    return angular.module('paxCosmica.constants', []);
});