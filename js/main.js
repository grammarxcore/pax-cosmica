// public/js/main.js

requirejs.config({
    baseUrl: "/",
    
    paths: {
        'app': 'js/app',
        'angular': 'libs/angular/angular',
        'ui-router': 'libs/ui-router/release/angular-ui-router',
        'jquery': 'libs/jquery/dist/jquery',
        'ui-bootstrap': 'libs/angular-bootstrap/ui-bootstrap-tpls',
        'ngStorage' : 'libs/ngstorage/ngStorage',
        'math': 'libs/mathjs/dist/math',
        'lz-string': 'libs/lz-string/libs/lz-string',
        'routes': 'js/appRoutes',
        'classes': 'js/classes/index',
        'constantsModule': 'js/constants/module',
        'services': 'js/services/index',
        'servicesModule': 'js/services/module',
        'controllers': 'js/controllers/index',
        'controllersModule': 'js/controllers/module'
    },
    
    shim: {
        'angular': {
            exports: 'angular'
        },
        'ui-router': {
            deps: ['angular']
        },
        'ui-bootstrap': {
            deps: ['angular']
        },
        'ngStorage': {
            deps: ['angular'],
            exports: 'ngStorage'
        },
        'lz-string': {
            exports: 'LZString'
        }
    },
    
    deps: ['js/bootstrap.js']
});