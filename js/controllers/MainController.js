// public/js/services/ShipController.js

define ([
		'angular',
		'math',
		'controllersModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.controllers').controller('MainController', [
		'Save',
		'Init',
		'Prestige',
		'$scope',
		'$state',
		function(
			Save,
			Init,
			Prestige,
			$scope,
			$state
		){

		$scope.getVersion = function(){
			return Save.getVersion();
		}

		$scope.save = function(){
			Save.save();
		}

		$scope.exportSave = function(){
			$scope.saveArea = Save.createSaveString();
		}

		$scope.importSave = function(){
			Save.loadSaveString($scope.saveArea);
		}

		$scope.canPrestige = function(){
			return Prestige.canPrestige();
		}

		$scope.prestige = function(){
			Prestige.prestige();
		}

		$scope.reset = function(){
			localStorage.clear();
			$state.go('index');
		}

	}]);


});