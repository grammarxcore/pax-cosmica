// public/js/controllers/module.js

define([
	'angular'
	], function (
		angular
	) {
    'use strict';

    return angular.module('paxCosmica.controllers', ['paxCosmica.services']);
});