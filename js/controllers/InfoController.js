// public/js/services/TerritoryController.js

define ([
		'angular',
		'math',
		'controllersModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.controllers').controller('InfoController', [
		'Save',
		'Tier',
		'$scope',
		'$interval',
		function(
			Save,
			Tier,
			$scope,
			$interval
		){

		// Playing since functions
		$scope.getStartDate = function(){
			return Save.getStartDate().toDateString();
		}

		$scope.getPlayingFor = function(){
			var date = new Date();
			var millis = date.getTime() - Save.getStartDate().getTime();
			var seconds = math.floor(millis / 1000);
			var minutes = 0;
			if (seconds > 60){
				minutes = math.floor(seconds / 60);
				seconds %= 60;
			}
			var hours = 0;
			if (minutes > 60){
				hours = math.floor(minutes / 60);
				minutes %= 60;
			}
			var string = "";
			if (hours >= 1){
				string += hours + " hour";
			}
			if (hours > 1){
				string += "s";
			}
			if (hours > 0){
				string += ", ";
			}
			if (minutes >= 1){
				string += minutes + " minute";
			}
			if (minutes > 1){
				string += "s";
			}
			if (minutes > 0){
				string += ", "
			}
			string += seconds + " second";
			if (seconds > 1){
				string += "s";
			}
			return string;

		}

		// Completed functions
		$scope.getCompleted = function(){
			return Tier.getCompleted();
		}

		$scope.hasCompleted = function(index){
			if (!(typeof index === 'undefined')){
				if (math.larger(Tier.getCompleted()[index], 0)){
					return true;
				} else {
					return false
				}
			} else {
				var completed = false;
				Tier.getCompleted().forEach(function(tier){
					if (math.larger(tier, 0)){
						completed = true;
						return;
					}
				})
				return completed;
			}
		}

		$scope.getCompletedName = function(index){
			if (math.equal($scope.getCompleted()[index], 1)) {
				return Tier.getTypes()[index];
			} else {
				return Tier.getPluralTypes()[index];
			}
		}

	}]);

})