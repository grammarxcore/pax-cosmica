// public/js/controllers/index.js

define([

	'js/controllers/InfoController',
	'js/controllers/MainController',
	'js/controllers/ShipController',
	'js/controllers/TerritoryController',
	'js/controllers/TerritoryListController'
  
], function () {});