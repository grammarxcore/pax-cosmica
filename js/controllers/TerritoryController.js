// public/js/services/TerritoryController.js

define ([
		'angular',
		'math',
		'controllersModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.controllers').controller('TerritoryController', [
		'Tier',
		'$stateParams',
		'$state',
		'$scope',
		'$interval',
		function(
			Tier,
			$stateParams,
			$state,
			$scope,
			$interval
		){

		if ((typeof $stateParams.tier === 'undefined') && (typeof $stateParams.index === 'undefined')){
			$scope.tier = 0;
			$scope.index = 0;	
		} else {
			$scope.tier = $stateParams.tier;
			$scope.index = $stateParams.index;
			if (typeof Tier.getTier($scope.tier, $scope.index) === 'undefined'){
				$state.transitionTo('territory');
			}
		}
		
		// Tier Functions
		$scope.getObjective = function(){
			return Tier.getTier($scope.tier, $scope.index).getObjective();
		}
		$scope.autocomplete = {};
		$scope.autocomplete.counter = $scope.getObjective().counter;
		$scope.autocomplete.max = $scope.getObjective().max;

		$scope.getObjectiveString = function(){
			return Tier.getTier($scope.tier, $scope.index).getObjectiveString();
		}

		$scope.getName = function(){
			return Tier.getTier($scope.tier, $scope.index).getName();
		}

		$scope.isCompleted = function(){
			return Tier.getTier($scope.tier, $scope.index).isCompleted();
		}

		$scope.getType = function(){
			return Tier.getTier($scope.tier, $scope.index).getType();
		}

		$scope.getPluralType = function(){
			return Tier.getPluralTypes()[$scope.tier];
		}

		$scope.getCurrency = function(){
			return Tier.getTier($scope.tier, $scope.index).getCurrency();
		}

		$scope.getCurrencyPerSecond = function(){
			return math.round(
				Tier.getTier($scope.tier, $scope.index).productionPerSecond(),
				5
			)
		}

		$scope.getUnits = function(){
			if (!(typeof Tier.getTier($scope.tier, $scope.index) === 'undefined')){
				return Tier.getTier($scope.tier, $scope.index).getUnits();	
			}
		}
		$scope.units = $scope.getUnits();

		$scope.getUnitClass = function(index){
			if ($scope.canBuy(index)){
				return "purchaseable";
			} else {
				return "not-purchaseable";
			}
		}

		$scope.canBuy = function(index){
			return Tier.getTier($scope.tier, $scope.index).canBuy(index)
		}

		$scope.buy = function(index){
			if ($scope.canBuy(index)){
				Tier.getTier($scope.tier, $scope.index).buy(index);
			}
		}

		$scope.complete = function(){
			Tier.complete($scope.tier, $scope.index);
			if (Tier.getTiers()[$scope.tier].length == 0){
				$scope.tier = 0;
				$scope.index = 0;
			}
			$state.transitionTo(
				'territory',
				{"tier": $scope.tier, "index": $scope.index}
			)
		}

		$interval(function(){
			$scope.units = $scope.getUnits();
			$scope.autocomplete.counter = $scope.getObjective().counter;
			$scope.autocomplete.max = $scope.getObjective().max;
		}, 100);


		// Research Functions
		 
		$scope.getResources = function(){
			return Tier.getResources($scope.tier);
		}
		 
		$scope.getResourceName = function(){
			return Tier.getResources($scope.tier).name;
		}

		$scope.getResourceAmount = function(){
			return Tier.getResources($scope.tier).amount;
		}
		 
		$scope.getGeneralResearch = function(){
			return Tier.getGeneralResearch($scope.tier);
		}

		$scope.getGeneralNext = function(index){
			return Tier.getGeneralNext($scope.tier, index);
		}

		$scope.getGeneralCost = function(name){
			return Tier.getGeneralCost($scope.tier, name);
		}

		$scope.canBuyGeneralResearch = function(name){
			if (math.largerEq(
				$scope.getResources().amount,
				$scope.getGeneralCost(name)
			)) {
				return true;
			} else {
				return false;
			}
		}

		$scope.getGeneralClass = function(name){
			if ($scope.canBuyGeneralResearch(name)){
				return "purchaseable";
			} else {
				return "not-purchaseable";
			}

		}

		$scope.buyGeneralResearch = function(name){
			if ($scope.canBuyGeneralResearch(name)){
				Tier.buyGeneralResearch($scope.tier, name);	
			}
		}

		$scope.getUnitResearch = function(){
			return Tier.getUnitResearch($scope.tier);
		}

		$scope.getUnitResearchValue = function(unit, name){
			return Tier.getUnitResearch($scope.tier)[unit][name].value;
		}

		$scope.getUnitResearchPercent = function(unit, name){
			return math.round(math.multiply(Tier.getUnitResearch($scope.tier)[unit][name].value, 100), 2);
		}

		$scope.getUnitName = function(index){
			return Tier.getTier($scope.tier, $scope.index).getPluralNames()[index];
		}

		$scope.getUnitNext = function(unit, name){
			return Tier.getUnitNext($scope.tier, unit, name);
		}

		$scope.getUnitNextPercent = function(unit, name){
			return math.round(math.multiply(Tier.getUnitNext($scope.tier, unit, name), 100), 2);
		}

		$scope.getUnitCost = function(unit, name){
			return Tier.getUnitCost($scope.tier, unit, name);
		}

		$scope.getExpensive = function(name){
			return Tier.getExpensive(name);
		}

		$scope.getSeconds = function(millis){
			return math.round(math.divide(millis, 1000), 2);
		}

		$scope.canBuyUnitResearch = function(unit, name){
			if (math.largerEq(
				$scope.getResources().amount,
				$scope.getUnitCost(unit, name)
			)){
				return true;
			} else {
				return false;
			}
		}

		$scope.getUnitClassResearch = function(unit, name, expensive){
			if ($scope.canBuyUnitResearch(unit, name, expensive)){
				return "purchaseable";
			} else {
				return "not-purchaseable";
			}
		}

		$scope.buyUnitResearch = function(unit, name, expensive){
			if ($scope.canBuyUnitResearch(unit, name, expensive)){
				Tier.buyUnitResearch($scope.tier, unit, name, expensive);
			}
		}
		

		$scope.generalCollapsed = false;
		$scope.collapseUnit = [];
		for (var i = 0; i < $scope.getUnitResearch().length; i++){
			$scope.collapseUnit.push(false);
		}
		$scope.collapse = function(index){
			switch(index){
				case 'general':
					if ($scope.generalCollapsed){
						$scope.generalCollapsed = false
					} else {
						$scope.generalCollapsed = true;
					}
					break;
				default:
					if ($scope.collapseUnit[parseInt(index)]){
						$scope.collapseUnit[parseInt(index)] = false;
					} else {
						$scope.collapseUnit[parseInt(index)] = true;
					}
			}
		}

		$scope.isCollapsed = function(index){
			switch(index){
				case 'general':
					return $scope.generalCollapsed;
					break;
				default:
					return $scope.collapseUnit[parseInt(index)];
					break;
			}
		}

		//TODO: Remove
		$scope.cheat = function(){
			return Tier.getTier($scope.tier, $scope.index).cheat();
		}

		$scope.cheatGalaxy = function(){
			Tier.cheatGalaxy();
			return Tier.getTier($scope.tier, $scope.index).cheat();
		}

	}]);


});