// public/js/services/ShipController.js

define ([
		'angular',
		'math',
		'controllersModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.controllers').controller('ShipController', [
		'Ship',
		'Tier',
		'$scope',
		'$state',
		'$interval',
		function(
			Ship,
			Tier,
			$scope,
			$state,
			$interval
		){

		var self = this;

		// Tab functions
		$scope.hasCompleted = function(index){
			if (math.largerEq(Tier.getCompleted(), 0)){
				return true;
			} else {
				return false;
			}
		}

		// Main functions
		$scope.getSeconds = function(min, max){
			return math.round(math.divide(math.subtract(max, min), 1000), 2) + "";
		}

		$scope.getComponents = function(){
			return Ship.getComponents();
		}

		$scope.turnComponentOn = function(index){
			if (!Ship.getComponents()[index].on){
				Ship.toggleComponent(index);
			}
		}

		// Research Functions
		// 
		$scope.getResource = function(index){
			return Tier.getResources(index);
		}
		
		$scope.getResearch = function(){
			return Ship.getResearch();
		}

		$scope.getResearchCost = function(index, name){
			return Ship.getResearchCost(index, name).toString();
		}

		$scope.getClass = function(index, name){
			index = parseInt(index);
			if (Ship.canBuy(index, name)){
				return "purchaseable";
			} else {
				return "not-purchaseable";
			}		
		}

		$scope.buyResearch = function(index, name){
			if (Ship.canBuy(index, name)){
				Ship.buyResearch(index, name);
			}
		}
		
		$scope.collapseGeneral = false;
		$scope.collapseComponents = [];
		for (var i = 0; i < $scope.getComponents().length; i++){
			$scope.collapseComponents.push(false);
		}
		$scope.collapse = function(index){
			switch(index){
				case 'general':
					$scope.collapseGeneral = !$scope.collapseGeneral;
					break;
				default:
					$scope.collapseComponents[parseInt(index)] = 
						!$scope.collapseComponents[parseInt(index)];
					break;
			}
		}

		$scope.isCollapsed = function(index){
			switch (index){
				case 'general':
					return $scope.collapseGeneral;
					break;
				default:
					return $scope.collapseComponents[parseInt(index)];
					break;
			}
		}

	}]);


});