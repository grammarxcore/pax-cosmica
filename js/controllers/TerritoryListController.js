// public/js/services/TerritoryController.js

define ([
		'angular',
		'math',
		'controllersModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.controllers').controller('TerritoryListController', [
		'Tier',
		'$scope',
		'$interval',
		function(
			Tier,
			$scope,
			$interval
		){

		var collapseTier = [];
		for (var i = 0; i < Tier.getTiers().length; i++){
			collapseTier.push(false);
		}

		$scope.getTiers = function(){
			return Tier.getTiers();
		}

		$scope.haveTier = function(index){
			if (Tier.getTiers()[index].length > 0){
				return true;
			} else {
				return false;
			}
		}

		$scope.getPluralName = function(index){
			return Tier.getPluralTypes()[index];
		}

		$scope.collapse = function(index){
			if (collapseTier[index]){
				collapseTier[index] = false;
			} else {
				collapseTier[index] = true;
			}
		}

		$scope.isCollapsed = function(index){
			return collapseTier[index];
		}
	}]);


});