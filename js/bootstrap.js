// public/js/bootstrap.js

define([
		'angular',
		'app',
		'routes'
	], function(
		angular
	){

	'use strict';
	
	// NOTE: Debugging only
	localStorage.clear();
  fetchData().then(bootstrapApplication);

  function fetchData() {
    var initInjector = angular.injector(['ng']);
    var $q = initInjector.get('$q');
    var $http = initInjector.get('$http');

    return $q.all([
      $http.get('/js/json/tiers.json').then(function(res){
        angular.module('paxCosmica.constants').constant('tiersArray', res.data);
      }),
      $http.get('/js/json/names.json').then(function(res){
        angular.module('paxCosmica.constants').constant('namesArray', res.data);
      })
  	]);
  }

  function bootstrapApplication() {
    angular.element(document).ready(function() {
        angular.bootstrap(document, [
          'paxCosmica',
          'paxCosmica.constants',
          'paxCosmica.services',
          'paxCosmica.controllers'          
        ]);
    });
  }

});