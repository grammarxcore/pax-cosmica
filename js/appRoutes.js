// public/js/appRoutes.js

define([
		'angular',
		'app'
	], function(
		angular,
		app
	){

	'use strict';

	return app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
	 function($stateProvider, $urlRouterProvider, $locationProvider){

	 	$stateProvider
			.state('index', {
				url: '/',
				templateUrl: '/views/Info.html'
			})
			.state('menu', {
				url: '/Menu',
				views: {
					'': {
						templateUrl: '/views/Menu.html'
					},
					'container@menu': {
						templateUrl: '/views/tabs/MenuSave.html'
					}
				}
			})
			.state('menu.save', {
				url: '',
				views: {
					'container': {
						templateUrl: '/views/tabs/MenuSave.html'
					}
				}
			})
			.state('menu.reset', {
				url: '',
				views: {
					'container': {
						templateUrl: '/views/tabs/MenuReset.html'
					}
				}
			})
			.state('menu.log', {
				url: '',
				views: {
					'container': {
						templateUrl: '/views/tabs/MenuLog.html'
					}
				}
			})
			.state('ship', {
				url: '/Ship',
				views: {
					'': {
						templateUrl: '/views/Ship.html'
					},
					'container@ship': {
						templateUrl: '/views/ship/Main.html'
					}
				}
			})
			.state('ship.main', {
				url: '/Main',
				views: {
					'container': {
						templateUrl: '/views/ship/Main.html'
					}
				}
			})
			.state('ship.research', {
				url: '/Research',
				views: {
					'container': {
						templateUrl: '/views/ship/Research.html'
					}
				}
			})
			.state('ship.crew', {
				url: '/Crew',
				views: {
					'container': {
						templateUrl: '/views/ship/Crew.html'
					}
				}
			})
			.state('ship.cargo', {
				url: '/Cargo',
				views: {
					'container': {
						templateUrl: '/views/ship/Cargo.html'
					}
				}
			})
			.state('ship.engine', {
				url: '/Engine',
				views: {
					'container': {
						templateUrl: '/views/ship/Engine.html'
					}
				}
			})
			.state('ship.armor', {
				url: '/Armor',
				views: {
					'container': {
						templateUrl: '/views/ship/Armor.html'
					}
				}
			})
			.state('ship.shields', {
				url: '/Shields',
				views: {
					'container': {
						templateUrl: '/views/ship/Shields.html'
					}
				}
			})
			.state('ship.weapons', {
				url: '/Weapons',
				views: {
					'container': {
						templateUrl: '/views/ship/Weapons.html'
					}
				}
			})
			.state('territory', {
				url: '/Territory?tier&index',
				views: {
					'': {
						templateUrl: '/views/Territory.html'
					},
					'territoryList@territory' : {
						templateUrl: '/views/TerritoryList.html'
					},
					'territoryMain@territory' : {
						templateUrl: '/views/TerritoryMain.html'
					},
					'container@territory': {
						templateUrl: '/views/tabs/TerritoryMain.html'
					}
				}
			})
			.state('territory.main', {
				url: '',
				views: {
					'container': {
						templateUrl: '/views/tabs/TerritoryMain.html'
					}
				}
			})
			.state('territory.research', {
				url: '',
				views: {
					'container': {
						templateUrl: '/views/tabs/TerritoryResearch.html'
					}
				}
			})

			$urlRouterProvider.otherwise('/');

			$locationProvider.html5Mode(true);

	}]);
})