// public/js/services/SaveService.js

define ([
		'angular',
		'lz-string',
		'servicesModule',
	], function(
		angular,
		LZString
	){

	'use strict';

	angular.module('paxCosmica.services').service('Save', [
		'$localStorage',
		'$rootScope',
		function(
			$localStorage,
			$rootScope
		){

		var self = this;

		var VERSION_NUMBER = "0.1.01";
		var startDate = new Date();

		/**
		 * Loads data pertinent to Save Service
		 */
		self.loadStorage = function(){
			if (typeof $localStorage["startDate"] === 'undefined'){
				$localStorage["startDate"] = startDate;
			}
			if (typeof $localStorage["versionNumber"] === 'undefined'){
				$localStorage["versionNumber"] = VERSION_NUMBER;
			}

		}

		self.getStartDate = function(){
			return startDate;
		}

		self.getVersion = function(){
			return VERSION_NUMBER;
		}

		/**
		 * Saves data pertinent to Save Service
		 */
		self.internalSave = function(){

		}

		/**
		 * Saves all the data from all the services
		 */
		self.save = function(){
			$rootScope.$broadcast('save');
			self.internalSave();
		}

		/**
		 * Creates a save array with all the information from Save
		 * @return {Array} the save array
		 */
		self.createInternalSaveString = function(){
			var saveArray = {"name": "Save", "variables": {
				"versionNumber": VERSION_NUMBER
			}}
			return saveArray;
		}

		/**
		 * Creates a base64 exportable string with all the save data from everywhere
		 * @return {String} An LZString-encoded string with all the information
		 */
		self.createSaveString = function(){
			var saveArray = [];
			$rootScope.saveArray = [];
			$rootScope.$broadcast('saveString');
			$rootScope.saveArray.push(self.createInternalSaveString());

			return LZString.compressToEncodedURIComponent(JSON.stringify($rootScope.saveArray));
		}

		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadInternalSaveString = function(saveArray){
			Object.keys(saveArray).forEach(function(key){
				$localStorage[key] = saveArray[key];
			});

			self.loadStorage();
		}

		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadSaveString = function(saveString){
			$rootScope.saveArray = JSON.parse(
				LZString.decompressFromEncodedURIComponent(saveString)
			);
			$rootScope.$broadcast('loadSaveString');
			self.loadInternalSaveString($rootScope.saveArray.filter(function(el){
				return el.name == 'Save'
			})[0].variables);
			//location.reload();
		}

		/**
		 * Saves every month
		 */
		$rootScope.$on('monthChange', function(){
			self.save();
		})


	}]);


});