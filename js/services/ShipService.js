// public/js/services/Service.js
// TODO: change name

define ([
		'angular',
		'math',
		'servicesModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.services').service('Ship', [
		'Tier',
		'$localStorage',
		'$rootScope',
		'$interval',
		function(
			Tier,
			$localStorage,
			$rootScope,
			$interval
		){

		var self = this;
		var INITIAL_TIME = 300000;
		var NUMBER_OF_COMPONENTS = 6;
		var COMPONENT_NAMES = [
			"Crew",
			"Cargo",
			"Engine",
			"Armor",
			"Shields",
			"Weapons"
		];
		var CREW = 0;
		var CARGO = 1;
		var ENGINE = 2;
		var ARMOR = 3;
		var SHIELDS = 4;
		var WEAPONS = 5;

		// TODO: initialize all variables
		var shipLoadDate = new Date();
		var date = new Date();
		
		var components = [];
		var research = [];
		for (var i = 0; i < COMPONENT_NAMES.length; i++){
			components.push({
				"name": COMPONENT_NAMES[i],
				"value": math.bignumber(0),
				"on": false,
				"counter": 0,
				"max": INITIAL_TIME,
				"auto": false
			});
			research.push({
				"autocomplete": {
					"level": math.bignumber(0),
					"value": false
				},
				"length": {
					"level": math.bignumber(0),
					"value": INITIAL_TIME,
					"isLow": false
				}
			})
		}

		// TODO: add all localStorage
		/**
		 * Loads data pertinent to this service
		 */
		self.loadStorage = function(){
			if (typeof $localStorage["shipLoadDate"] === 'undefined'){
				$localStorage["shipLoadDate"] = shipLoadDate;
			}

			shipLoadDate = new Date($localStorage["shipLoadDate"]);

			if (typeof $localStorage["components"] === 'undefined'){
				$localStorage["components"] = components;
			}
			components = $localStorage["components"];

		}
		self.loadStorage();

		// Getters
		
		self.getComponent = function(name){
			var index = 0;
			switch (name){
				case CREW:
				case "crew":
					index = CREW;
					break;
				case CARGO:
				case "cargo":
					index = CARGO;
					break;
				case ENGINE:
				case "engine":
					index = ENGINE;
					break;
				case ARMOR:
				case "armor":
					index = ARMOR;
					break;
				case SHIELDS:
				case "shields":
					index = SHIELDS;
					break;
				case WEAPONS:
				case "weapons":
					index = WEAPONS;
					break;
			}
			return components[index];
		}

		self.getComponents = function(){
			return components;
		}

		self.getResearch = function(){
			return research;
		}

		self.isExpensive = function(name){
			switch(name){
				case 'autocomplete':
					return true;
					break;
				default:
					return false;
					break;
			}
		}

		self.getResearchNext = function(index, name){
			switch(name){
				case 'autocomplete':
					return true;
					break;
			}
		}

		self.getResearchCost = function(index, name){
			var level = research[index][name].level;
			if (self.isExpensive(name)){
				return math.ceil(math.add(
					math.bignumber(99),
					math.pow(math.bignumber(11), level)
				))
			} else {
				return math.ceil(math.add(
					math.bignumber(10),
					math.pow(math.bignumber(1.75), level)
				))
			}
		}

		self.canBuy = function(index, name){
			if (math.largerEq(
				Tier.getResources(index).amount,
				self.getResearchCost(index, name)
			)){
				return true;
			} else {
				return false;
			}
		}

		self.buyResearch = function(index, name){
			if (self.canBuy(index, name)){
				Tier.getResources(index).amount = math.subtract(
					Tier.getResources(index).amount,
					self.getResearchCost(index, name)
				);
				research[index][name].value = self.getResearchNext(index, name);
				research[index][name].level = math.add(
					research[index][name].level,
					1
				);
				self.updateResearch(index, name)
			}
		}

		self.updateResearch = function(index, name){
			switch (name){
				case 'autocomplete':
					components[index].auto = true;
			}
		}

		self.toggleComponent = function(name){
			if (self.getComponent(name).on){
				self.getComponent(name).on = false;
			} else {
				self.getComponent(name).on = true;
			}

		}

		self.tickComponent = function(name, amount){
			self.getComponent(name).counter += parseInt(amount);
			if (math.largerEq(
				self.getComponent(name).counter,
				self.getComponent(name).max
			)) {
				var value = math.floor(math.divide(
					self.getComponent(name).counter,
					self.getComponent(name).max
				));
				self.getComponent(name).value = math.add(
					self.getComponent(name).value,
					value
				);
				self.getComponent(name).counter = math.mod(
					self.getComponent(name).counter,
					self.getComponent(name).max
				);
				if (!self.getComponent(name).auto){
					self.toggleComponent(name);
				}
			}
		}
		

		$interval(function(){
			date = new Date();
			if ((date.getTime() - shipLoadDate.getTime()) >= 1){
				var timeVar = parseInt(date.getTime() - shipLoadDate.getTime());
				for (var i = 0; i < COMPONENT_NAMES.length; i++){
					if (components[i].on || components[i].auto){
						self.tickComponent(i, timeVar);
					}
				}
				shipLoadDate = date;
			}

		}, 50)

		

		// TODO: add all variables
		/**
		 * Saves all the data from this service
		 */
		self.save = function(){
			$localStorage["shipLoadDate"] = shipLoadDate;
			$localStorage["components"] = components;
		}

		// TODO: add all variables
		/**
		 * Creates a save array with all the information from this service
		 */
		self.createSaveString = function(){
			var saveArray = {
				"name": "Ship",
				"variables": {
				"shipLoadDate": shipLoadDate,
				"components": components
			}};
			$rootScope.saveArray.push(saveArray);
		}
		$rootScope.$on('saveString', function(){
			self.createSaveString();
		})

		// TODO: fix any arrays
		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadSaveString = function(saveArray){
			Object.keys(saveArray).forEach(function(key){
				$localStorage[key] = saveArray[key]
			});

			self.loadStorage();
		}
		$rootScope.$on('loadSaveString', function(){
			self.loadSaveString($rootScope.saveArray.filter(function(el){
				return el.name == ''
			})[0].variables);
		})

		/**
		 * Saves on save broadcast
		 */
		$rootScope.$on('save', function(){
			self.save();
		})

	}]);


});