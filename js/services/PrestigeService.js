// public/js/services/PrestigeService.js

define ([
		'angular',
		'math',
		'servicesModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.services').service('Prestige', [
		'$state',
		'$localStorage',
		'$rootScope',
		'$modal',
		function(
			$state,
			$localStorage,
			$rootScope,
			$modal
		){

		var self = this;

		var prestige = math.bignumber(0);
		var prestigeContinue = false;

		/**
		 * Loads data pertinent to this service
		 */
		self.loadStorage = function(){
			if (typeof $localStorage["prestige"] === 'undefined'){
				$localStorage["prestige"] = 0;
			}
			prestige = math.bignumber($localStorage["prestige"])

			if (typeof $localStorage["prestigeContinue"] === 'undefined'){
				$localStorage["prestigeContinue"] = false;
			}
			prestigeContinue = $localStorage["prestigeContinue"];

		}
		self.loadStorage();

		// Getters
		 
		self.getPrestige = function(){
			return prestige;
		}

		self.getNeededGalaxies = function(){
			return math.pow(math.add(prestige, 1), 2);
		}

		self.canPrestige = function(){
			return prestigeContinue;
		}
		
		// Setters
		 
		// Action functions
		// 
		$rootScope.$on('allCompleted', function(){
			if (!prestigeContinue){
				prestigeContinue = true;
				$modal.open({
					templateUrl: '/views/modals/Prestige.html',
					controller: ['$scope', '$modalInstance', '$rootScope',
						function($scope, $modalInstance, $rootScope){

						$scope.getResearchLevel = function(){
							return math.add(prestige, 1).toString();
						}

						$scope.close = function(){
							$modalInstance.close();
						}

						$scope.continue = function(){
							$scope.close();
						}

						$scope.prestige = function(){
							$scope.close();
							self.prestige();
						}

					}],
					windowClass: 'center-modal'
				})
			}
		})
		
		self.prestige = function(){
			prestige = math.add(prestige, 1);
			prestigeContinue = false;
			self.save();
			$rootScope.$broadcast('prestige');
			$state.go('index');
		} 

		/**
		 * Saves all the data from this service
		 */
		self.save = function(){
			$localStorage["prestige"] = prestige;
			$localStorage["prestigeContinue"] = prestigeContinue;
			
		}

		/**
		 * Creates a save array with all the information from this service
		 */
		self.createSaveString = function(){
			var saveArray = {
				"name": "Prestige",
				"variables": {
				"prestige": prestige,
				"prestigeContinue": prestigeContinue 
			}};
			$rootScope.saveArray.push(saveArray);
		}
		$rootScope.$on('saveString', function(){
			self.createSaveString();
		})

		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadSaveString = function(saveArray){
			Object.keys(saveArray).forEach(function(key){
				$localStorage[key] = saveArray[key]
			});

			self.loadStorage();
		}
		$rootScope.$on('loadSaveString', function(){
			self.loadSaveString($rootScope.saveArray.filter(function(el){
				return el.name == 'Prestige';
			})[0].variables);
		})

		/**
		 * Saves on save broadcast
		 */
		$rootScope.$on('save', function(){
			self.save();
		})


	}]);


});