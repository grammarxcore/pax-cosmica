// public/js/services/index.js

define([  
	
	'js/services/InitService',
	'js/services/PrestigeService',
	'js/services/SaveService',
	'js/services/ShipService',
	'js/services/TierService',
	'js/services/YearService'
  
], function () {});