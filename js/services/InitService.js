// public/js/services/InitService.js
// TODO: change name

define ([
		'angular',
		'servicesModule',
	], function(
		angular
	){

	'use strict';

	angular.module('paxCosmica.services').service('Init', [
		'Save',
		'Tier',
		'$localStorage',
		'$rootScope',
		function(
			Save,
			Tier,
			$localStorage,
			$rootScope
		){

		var self = this;

		self.initialize = function(){
			Tier.addInitTier();
		}

		if (typeof $localStorage["initialized"] === 'undefined'){
			$localStorage["initialized"] = true;
			self.initialize();
		}

		$rootScope.$on('tiersInitialized', function(){
			self.initialize();
			Save.save();
		})

	}]);


});