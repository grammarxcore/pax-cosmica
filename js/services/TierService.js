// public/js/services/TierService.js

define ([
		'angular',
		'math',
		'NormalRandom',
		'Tier',
		'servicesModule',
	], function(
		angular,
		math,
		NormalRandom,
		Tier
	){

	'use strict';

	angular.module('paxCosmica.services').service('Tier', [
		'tiersArray',
		'namesArray',
		'Prestige',
		'$localStorage',
		'$rootScope',
		'$interval',
		function(
			tiersArray,
			namesArray,
			Prestige,
			$localStorage,
			$rootScope,
			$interval
		){

		var self = this;

		var TYPES = ["Continent", "Satellite", "Planet", "System", "Sector", "Galaxy"];
		var PLURAL_TYPES = ["Continents", "Satellites", "Planets", "Systems", "Sectors", "Galaxies"];
		// positions in tiers array
		var CONTINENT = 0;
		var SATELLITE = 1;
		var PLANET = 2;
		var SYSTEM = 3;
		var SECTOR = 4;
		var GALAXY = 5;
		// number of units
		var UNITS = 9;
		// amount of starting currency
		var START_CURRENCY = 10;

		var tierLoadDate;
		var date;
		var tiersProperties = tiersArray;
		var tiers;
		var tiersCompleted;
		var research;
		var resources;

		self.init = function(){
			tierLoadDate = new Date();
			date = tierLoadDate;
			tiers = [];
			tiersCompleted = [];
			for (var i = 0; i < tiersProperties.length; i++){
				tiers.push([]);
				tiersCompleted.push(math.bignumber(0));
			}
			resources = [
				{"name": "Food", "amount": math.bignumber(0)},
				{"name": "Pods", "amount": math.bignumber(0)},
				{"name": "Fuel", "amount": math.bignumber(0)},
				{"name": "Titanium", "amount": math.bignumber(0)},
				{"name": "Energy", "amount": math.bignumber(0)},
				{"name": "Lasers", "amount": math.bignumber(0)}
			];

			tiersProperties[GALAXY].total = Prestige.getNeededGalaxies();
		}
		self.init();

		/**
		 * All research is manually added here.
		 * 
		 * All research needs to be added to isExpensive (or not).
		 * 
		 * General research needs to be added getGeneralNext(name)
		 *
		 * Unit research needs to be added to getUnitNext(name)
		 *
		 * If the research affects Tier, it needs to be added to
		 * Tier.updateResearch()
		 *
		 * Manually add other research affects.	
		 * 
		 * @return {[type]} [description]
		 */
		self.initResearch = function(){
			var array = [];
			array = [];
			var level = Prestige.getPrestige();
			var percent = math.bignumber(1);
			var value;
			var isOne;
			var isLow;
			for (var i = 0; math.smaller(i, level); i++){
				percent = math.multiply(percent, 0.96);
			}
			for (var i = 0; i < tiersProperties.length; i++){
				array.push({});
			}
			array.forEach(function(tier){
				tier.generalResearch = {};
				tier.generalResearch.startingCurrency = {
					"value": math.multiply(math.bignumber(10), math.add(level, 1)),
					"level": level
				};
				tier.generalResearch.startingCurrencyRate = {
					"value": math.add(math.bignumber(10), level),
					"level": level
				};
				tier.generalResearch.autocomplete = {
					"value": false,
					"level": level
				};
				value = math.ceil(math.multiply(120000, percent));
				isOne = false;
				if (math.smaller(value, 1000)){
					value = math.bignumber(1000);
					isOne = true;
				}
				tier.generalResearch.autocompleteTime = {
					"value": value,
					"level": level,
					"isOne": isOne
				};
				tier.unitResearch = [];
				for (var i = 0; i < UNITS; i++){
					tier.unitResearch.push({});
				}
				tier.unitResearch.forEach(function(unit){
					value = percent;
					isLow = false;
					if (math.smaller(value, 0.01)){
						value = math.bignumber(0.01);
						isLow = true;
					}
					unit.priceMultiplier = {
						"value": value,
						"level": level,
						"isLow": isLow
					};
					unit.startingNumber = {
						"value": math.add(math.bignumber(0), level),
						"level": level
					}
					unit.timeMultiplier = {
						"value": value,
						"level": level,
						"isLow": isLow
					};
				})
			});
			return array;
		}

		/**
		 * Loads data pertinent to this service
		 */
		self.loadStorage = function(){
			if (typeof $localStorage["tierLoadDate"] === 'undefined'){
				$localStorage["tierLoadDate"] = tierLoadDate;
			}
			tierLoadDate = new Date($localStorage["tierLoadDate"]);

			if (typeof $localStorage["tiersProperties"] === 'undefined'){
				$localStorage["tiersProperties"] = tiersProperties;
			}
			tiersProperties = $localStorage["tiersProperties"];

			if (typeof $localStorage["tiersCompleted"] === 'undefined'){
				$localStorage["tiersCompleted"] = tiersCompleted;
			}
			for (var i = 0; i < tiersProperties.length; i++){
				tiersCompleted[i] = math.bignumber($localStorage["tiersCompleted"][i]);
			}
			
			if (typeof $localStorage["research"] === 'undefined'){				
				$localStorage["research"] = self.initResearch();
			}
			research = $localStorage["research"];

			if (typeof $localStorage["tiers"] === 'undefined'){
				$localStorage["tiers"] = tiers;
			}

			var counter = 0;
			tiers = [];
			for (var i = 0; i < tiersProperties.length; i++){
				tiers.push([]);
			}
			$localStorage["tiers"].forEach(function(tierArray){
				if (tierArray.length > 0){
					tierArray.forEach(function(tier){
						tiers[counter].push(new Tier(
							tier.name,
							tier.type,
							tier.units,
							research[counter],
							tier.currency,
							tier.objective
						));
					})
				}
				counter++;
			})

			if (typeof $localStorage["resources"] === 'undefined'){
				$localStorage["resources"] = resources;
			}
			resources = $localStorage["resources"];
			resources.forEach(function(item){
				item.amount = math.bignumber(item.amount);
			})
		}
		self.loadStorage();

		// Getters
		
		self.getTypes = function(){
			return TYPES;
		}

		self.getPluralTypes = function(){
			return PLURAL_TYPES;
		}

		self.getTiers = function(){
			return tiers;
		}
		
		self.getTier = function(tier, index){
			return tiers[tier][index];
		}

		self.getCompleted = function(){
			return tiersCompleted;
		}

		self.checkIsOn = function(tier, unitIndex){
			if (tier.haveUnit(unitIndex)){
				return true;
			} else {
				return false;
			}
		}

		self.getCounter = function(tier, unitIndex){
			return tier.getCounter(unitIndex);
		}

		self.getMax = function(tier, unitIndex){
			return tier.getMax(unitIndex);
		}

		self.getGeneralResearch = function(tier){
			return research[tier].generalResearch;
		}

		self.getGeneralNext = function(tier, index){
			switch (index){
				case 'autocomplete':
					return true;
					break;
				case 'autocompleteTime':
					var value = math.ceil(math.multiply(
						research[tier].generalResearch.autocompleteTime.value,
						0.96
					));
					if (math.smaller(value, 1000)){
						research[tier].generalResearch.autocompleteTime.isOne = true;
					}
					return value;
					break;
				case 'startingCurrency':
					return math.add(
						research[tier].generalResearch.startingCurrency.value,
						research[tier].generalResearch.startingCurrencyRate.value
					)
					break;
				case 'startingCurrencyRate':
					return math.add(
						research[tier].generalResearch.startingCurrencyRate.value,
						1
					)
					break;
			}
		}

		self.getGeneralCost = function(tier, name){
			tier = parseInt(tier);
			var level = research[tier].generalResearch[name].level;
			if (self.isExpensive(name)){
				return math.ceil(math.add(
					math.bignumber(99),
					math.pow(math.bignumber(11), level)
				));
			} else {
				return math.ceil(math.add(
					math.bignumber(10),
					math.pow(math.bignumber(1.75), level)
				));
			}	
		}

		self.getUnitResearch = function(tier){
			return research[tier].unitResearch;
		}

		self.getUnitNext = function(tier, unit, name){
			var value = research[tier].unitResearch[unit][name].value;
			switch (name){
				case 'startingNumber':
					return math.add(value, 1);
					break;
				case 'timeMultiplier':
				case 'priceMultiplier':
				default:
					var number = math.multiply(value, 0.96);
					if (math.smallerEq(number, 0.01)){
						research[tier].unitResearch[unit][name].isLow = true;
						number = math.bignumber(0.01);
					}
					return number;
					break;
			}
			
		}

		self.getUnitCost = function(tier, unit, name){
			var level = research[tier].unitResearch[unit][name].level;
			if (self.isExpensive(name)){
				return math.ceil(math.add(
					math.bignumber(99),
					math.pow(math.bignumber(11), level)
				))
			} else {
				return math.ceil(math.add(
					math.bignumber(10),
					math.pow(math.bignumber(1.75), level)
				))
			}
		}

		self.isExpensive = function(name){
			switch (name){
				case 'autocomplete':
				case 'startingNumber':
				case 'timeMultiplier':
					return true;
					break;
				default:
					return false;
					break;
			}
		}

		self.getResources = function(index){
			return resources[index];
		}

		self.generateName = function(index){
			switch (parseInt(index)){
				case CONTINENT:
					var spot = Math.round(
						Math.random() * (namesArray.emperors.length - 1)
					);
					return namesArray.emperors[spot];
					break;
				case SATELLITE:
					var spot = Math.round(
						Math.random() * (namesArray.emperors.length - 1)
					);
					var secondary = Math.round(
						Math.random() * (namesArray.numerals.length - 1)
					)
					return namesArray.emperors[spot]
						+ " "
						+ namesArray.numerals[secondary];
					break;
				case PLANET:
					var spot = Math.round(
						Math.random() * (namesArray.gods.length - 1)
					);
					var secondary = Math.round(
						Math.random() * (namesArray.numerals.length - 1)
					)
					return namesArray.gods[spot]
						+ " "
						+ namesArray.numerals[secondary];
					break;
				case SYSTEM:
					var spot = Math.round(
						Math.random() * (namesArray.gods.length - 1)
					);
					return namesArray.gods[spot];
					break;
				case SECTOR:
					var spot = Math.round(
						Math.random() * (namesArray.greek.length - 1)
					);
					var secondary = Math.round(
						Math.random() * (namesArray.zodiac.length - 1)
					)
					return namesArray.greek[spot]
						+ " "
						+ namesArray.zodiac[secondary];
					break;
				case GALAXY:
					var spot = Math.round(
						Math.random() * (namesArray.zodiac.length - 1)
					);
					return namesArray.zodiac[spot];
					break;
			}
		}

		self.addInitTier = function(){
			tiers[CONTINENT].push(new Tier(
				self.generateName(0),
				TYPES[0],
				[],
				research[CONTINENT],
				research[CONTINENT].generalResearch.startingCurrency.value
			))
		}

		self.addTier = function(tier){
			tier = parseInt(tier);
			tiers[tier].push(new Tier(
				self.generateName(tier),
				TYPES[tier],
				[],
				research[tier],
				research[tier].generalResearch.startingCurrency.value
			))
		}

		self.updateResearch = function(index){
			tiers[index].forEach(function(tier){
				tier.updateResearch(research[index]);
			})
		}

		self.buyGeneralResearch = function(tier, name){
			tier = parseInt(tier);
			if (math.largerEq(
				resources[tier].amount,
				self.getGeneralCost(tier, name)
			)){
				resources[tier].amount = math.subtract(
					resources[tier].amount, 
					self.getGeneralCost(tier, name)
				)
				self.increaseGeneralResearch(tier, name);
				self.updateResearch(tier);
			}
			
		}

		self.increaseGeneralResearch = function(tier, name){
			research[tier].generalResearch[name].value = self.getGeneralNext(
				tier,
				name
			);
			switch (name){

			}
			research[tier].generalResearch[name].level = math.add(
				research[tier].generalResearch[name].level,
				1
			);
		}

		self.buyUnitResearch = function(tier, unit, name){
			tier = parseInt(tier);
			unit = parseInt(unit);
			if (math.largerEq(
				resources[tier].amount,
				self.getUnitCost(tier, unit, name)
			)) {
				resources[tier].amount = math.subtract(
					resources[tier].amount,
					self.getUnitCost(tier, unit, name)
				);
				self.increaseUnitResearch(tier, unit, name);
				self.updateResearch(tier);
			}
		}

		self.increaseUnitResearch = function(tier, unit, name){
			research[tier].unitResearch[unit][name].value = self.getUnitNext(
				tier,
				unit,
				name
			);
			research[tier].unitResearch[unit][name].level = math.add(
				research[tier].unitResearch[unit][name].level,
				1
			)
		}

		self.complete = function(tier, index){
			tier = parseInt(tier);
			index = parseInt(index);
			// TODO: change
			var update = 1000;
			//var update = math.floor(NormalRandom(1,10));
			resources[tier].amount = math.add(resources[tier].amount, update);
			tiers[tier].splice(index, 1);
			if (tier == 0){
				self.addTier(tier);	
			}
			tiersCompleted[tier] = math.add(tiersCompleted[tier], 1);
			if (math.equal(tiersCompleted[GALAXY], Prestige.getNeededGalaxies())){
				$rootScope.$broadcast('allCompleted');
			}
			if ((math.smaller(tiers[tier].length, tiersProperties[tier].total))
				&& (tier == 0)) {
				self.addTier(tier, tiers[tier].length);
			}
			if (math.equal(
				math.mod(tiersCompleted[tier], tiersProperties[tier].amountToNext),
				0
			)){
				if (tier < tiersProperties.length){
					if ((tiers[tier + 1].length < tiersProperties[tier + 1].total)
						|| (tier == GALAXY)){
						self.addTier(tier + 1, tiers[tier + 1].length);
					}
				}
			}
		}
		
		self.tick = function (number){
			tiers.forEach(function(tierArray){
				if (tierArray.length > 0){
					tierArray.forEach(function(tier){
						tier.increaseCounter(number);
						if (tier.getObjective().autocompleteFinished){
							self.complete(tiers.indexOf(tierArray), tierArray.indexOf(tier));
						}
					})
				}
			})
		}

		$interval(function(){
			date = new Date();
			if ((date.getTime() - tierLoadDate.getTime()) >= 1){
				var timeVar = parseInt(date.getTime() - tierLoadDate.getTime());
				self.tick(timeVar);
				tierLoadDate = date;
			}

		}, 1)

		$rootScope.$on('prestige', function(){
			self.init();
			research = self.initResearch();
			self.save();
			$rootScope.$broadcast('tiersInitialized');
		})

		self.generateTierSaveArray = function(){
			var saveArray = [];
			for (var i = 0; i < tiersProperties.length; i++){
				saveArray.push([]);
				if (tiers[i].length > 0){
					tiers[i].forEach(function(tier){
						saveArray[i].push(tier.getSaveString())
					})
				}
			}
			return saveArray;
		}
		/**
		 * Saves all the data from this service
		 */
		

		self.save = function(){
			$localStorage["tierLoadDate"] = tierLoadDate;
			$localStorage["tiersProperties"] = tiersProperties;
			$localStorage["tiers"] = self.generateTierSaveArray();
			$localStorage["tiersCompleted"] = tiersCompleted;
			$localStorage["research"] = research;
		}

		/**
		 * Creates a save array with all the information from this service
		 */
		self.createSaveString = function(){
			var saveArray = {
				"name": "Tiers",
				"variables": {
				"tierLoadDate": tierLoadDate,
				"tiersProperties": tiersProperties,
				"tiers": self.generateTierSaveArray(),
				"tiersCompleted": tiersCompleted,
				"research": research
			}};
			$rootScope.saveArray.push(saveArray);
		}
		$rootScope.$on('saveString', function(){
			self.createSaveString();
		})

		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadSaveString = function(saveArray){
			Object.keys(saveArray).forEach(function(key){
				$localStorage[key] = saveArray[key]
			});

			self.loadStorage();
		}
		$rootScope.$on('loadSaveString', function(){
			self.loadSaveString($rootScope.saveArray.filter(function(el){
				return el.name == 'Tiers'
			})[0].variables);
		})

		/**
		 * Saves on save broadcast
		 */
		$rootScope.$on('save', function(){
			self.save();
		})

		// TODO: Remove
		self.cheatGalaxy = function(){
			tiersCompleted[GALAXY] = Prestige.getNeededGalaxies();
		}


	}]);


});
