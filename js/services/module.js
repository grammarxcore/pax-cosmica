// public/js/services/module.js

define([
	'angular'
	], function (
		angular
	) {
    'use strict';

    return angular.module('paxCosmica.services', ['paxCosmica.constants']);
});