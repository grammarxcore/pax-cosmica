// public/js/services/Service.js
// TODO: change name

define ([
		'angular',
		'servicesModule',
	], function(
		angular
	){

	'use strict';

	angular.module('paxCosmica.services').service('', [
		'$localStorage',
		'$rootScope',
		function(
			$localStorage,
			$rootScope
		){

		var self = this;

		// TODO: initialize all variables


		// TODO: add all localStorage
		/**
		 * Loads data pertinent to this service
		 */
		self.loadStorage = function(){

		}
		self.loadStorage();

		// Getters
		
		// Setters
		 
		// Action functions

		// TODO: add all variables
		/**
		 * Saves all the data from this service
		 */
		self.save = function(){
			
		}

		// TODO: add all variables
		/**
		 * Creates a save array with all the information from this service
		 */
		self.createSaveString = function(){
			var saveArray = {
				"name": "",
				"variables": {
				"": 
			}};
			$rootScope.saveArray.push(saveArray);
		}
		$rootScope.$on('saveString', function(){
			self.createSaveString();
		})

		// TODO: fix any arrays
		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadSaveString = function(saveArray){
			Object.keys(saveArray).forEach(function(key){
				$localStorage[key] = saveArray[key]
			});

			self.loadStorage();
		}
		$rootScope.$on('loadSaveString', function(){
			self.loadSaveString($rootScope.saveArray.filter(function(el){
				return el.name == '';
			})[0].variables);
		})

		/**
		 * Saves on save broadcast
		 */
		$rootScope.$on('save', function(){
			self.save();
		})


	}]);


});