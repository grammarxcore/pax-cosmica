// public/js/services/YearService.js
// TODO: change name

define ([
		'angular',
		'math',
		'servicesModule',
	], function(
		angular,
		math
	){

	'use strict';

	angular.module('paxCosmica.services').service('Year', [
		'$rootScope',
		'$interval',
		function(
			$rootScope,
			$interval
		){

		// TODO: update to $localStorage

		// Initialize variables
		var START_MONTH = "Ianuarius";
		var self = this;

		// Changing date
		var date = new Date(); 
		// Monitors the load time
		var yearLoadDate = new Date();

		var year = new math.bignumber(0);
		var day = 1;
		var month = START_MONTH;
		var dayCounter = 1;

		/**
		 * Loads data pertinent to this service
		 */
		self.loadStorage = function(){
			if (typeof $localStorage["yearLoadDate"] === 'undefined'){
				$localStorage["yearLoadDate"] = date;
			}
			yearLoadDate = new Date($localStorage["yearLoadDate"]);

			if (typeof $localStorage["year"] === 'undefined'){
				$localStorage["year"] = 0;
			}
			year = math.bignumber($localStorage["year"]);

			if (typeof $localStorage["day"] === 'undefined'){
				$localStorage["day"] = 1;
			}
			day = $localStorage["day"];

			if (typeof $localStorage["dayCounter"] === 'undefined'){
				$localStorage["dayCounter"] = 1;
			}
			dayCounter = $localStorage["dayCounter"];

			if (typeof $localStorage["month"] === 'undefined'){
				$localStorage["month"] = START_MONTH;
			}
			month = $localStorage["month"];
		}
		self.loadStorage();

		/**
		 * Returns the game year
		 * @return {bignumber} the year
		 */
		self.getYear = function(){
			return year;
		}

		/**
		 * Returns the game month
		 * @return {Number} the month
		 */
		self.getMonth = function(){
			return month;
		}

		/**
		 * Return the game day of month
		 * @return {Number} the day of month
		 */
		self.getDay = function(){
			return dayCounter;
		}

		// Tick year
		/**
		 * Iterates through the game year
		 * @param  {Number} number The number of seconds to tick through, equal to 
		 * days.
		 */
		self.tickYear = function(number){
			for (var i = 0; i < number; i++){
				day++;
				dayCounter++;
				switch (day){
					case 1:
						month = "Ianuarius";
						dayCounter = 1;
						break;
					case 30:
						month = "Februarius";
						dayCounter = 1;
						break;
					case 58:
						month = "Martius";
						dayCounter = 1;
						break;
					case 89:
						month = "Aprilis";
						dayCounter = 1;
						break;
					case 118:
						month = "Maius";
						dayCounter = 1;
						break;
					case 149:
						month = "Iunius";
						dayCounter = 1;
						break;
					case 178:
						month = "Quintilis";
						dayCounter = 1;
						break;
					case 209:
						month = "Sextilis";
						dayCounter = 1;
						break;
					case 238:
						month = "September";
						dayCounter = 1;
						break;
					case 267:
						month = "October";
						dayCounter = 1;
						break;
					case 298:
						month = "November";
						dayCounter = 1;
						break;
					case 327:
						month = "December";
						dayCounter = 1;
						break;
					case 356:
						day = 1;
						month = "Ianuarius";
						dayCounter = 1;
						year = math.add(year, math.bignumber(1));
						$rootScope.$broadcast(
							'yearChange',
							{startYear: math.subtract(year, 1), endYear: year, number: 1}
						)
						break;
				}
				if (dayCounter == 1){
					$rootScope.$broadcast('monthChange');
				}

			}
		}

		/**
		 * Saves all the data from this service
		 */
		self.save = function(){
			$localStorage["yearLoadDate"] = yearLoadDate;
			$localStorage["year"] = year;
			$localStorage["day"] = day;
			$localStorage["dayCounter"] = dayCounter;
			$localStorage["month"] = month;
		}

		/**
		 * Creates a save array with all the information from this service
		 */
		self.createSaveString = function(){
			var saveArray = {"name": "Year", "variables": {
				"yearLoadDate": yearLoadDate,
				"year": year,
				"day": day,
				"month": month,
				"dayCounter": dayCounter
			}};
			$rootScope.saveArray.push(saveArray);
		}
		$rootScope.$on('saveString', function(){
			self.createSaveString();
		})

		// TODO: fix any arrays
		/**
		 * Loads all the data from an LZString-encoded input
		 * @param  {String} saveString Encoded save string
		 */
		self.loadSaveString = function(saveArray){
			Object.keys(saveArray).forEach(function(key){
				$localStorage[key] = saveArray[key];
			});

			self.loadStorage();
		}
		$rootScope.$on('loadSaveString', function(){
			self.loadSaveString($rootScope.saveArray.filter(function(el){
				return el.name == 'Year'
			})[0].variables);
		})

		/**
		 * Saves on save broadcast
		 */
		$rootScope.$on('save', function(){
			self.save();
		})

		var timeVar = 0;
		$interval(function(){
			date = new Date();
			if ((date.getTime() - yearLoadDate.getTime()) >= 1000){
				timeVar += parseInt(date.getTime() - yearLoadDate.getTime());
				if (timeVar >= 1000){
					self.tickYear(Math.floor(timeVar / 1000));
					timeVar %= 1000;
				}
				yearLoadDate = date;
			}
		}, 1000);

		// TODO: Remove all functions below self comment before release
		self.increaseYear = function(){
			year = math.add(year, 10);
			$rootScope.$broadcast(
				'yearChange',
				{startYear: math.subtract(year, 10), endYear: year, number: 10}
			);
			$rootScope.$broadcast('monthChange');
		}


	}]);


});